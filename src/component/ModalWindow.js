import React, { Component } from 'react';
import './ModalWindow.css';


class  ModalWindow extends Component{
    render() {

        let modal = (
            <div className="ModalStyles" >
                <div className="headerModal">
                <span className="colorWhite headerP">Do you want to delete this file?</span>
                <button className="modalBtnClose" onClick={this.props.onClose}>X</button>

                </div>

                <div>{this.props.children}</div>
                <a className="buttonModal colorWhite" onClick={this.props.onClose}>Ok</a>
                <a className="buttonModal colorWhite" onClick={this.props.onClose}>Cancel</a>
            </div>
        );


        if (! this.props.isOpen) {
            modal = null;
        }

        return (
       <div>
           {modal}
       </div>
        );
    }
}
export default ModalWindow;