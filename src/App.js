import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ModalWindow from './component/ModalWindow';

class App extends Component{
    state = {
        isOpen: false
    }
  render() {
    return (
     <div className="App">
         <button onClick={(e) => this.setState({isOpen: true})}>Open Modal</button>
         <ModalWindow isOpen= {this.state.isOpen} onClose={ (e) => this.setState({isOpen: false})}>
             <p className="content colorWhite">Once you delete this file, it won't be possible to undo this action.Are you sure you want to delete it?</p>



         </ModalWindow>
         <div>{this.props.children}</div>
     </div>
    );
  }
}

export default App;
